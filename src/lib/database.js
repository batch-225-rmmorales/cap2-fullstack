import mongoose from 'mongoose';
let db = mongoose.connection;

// 1 connected; 0 not connected
let dbStatus = 0;

export const dbConnect = async () => {
	if (dbStatus == 1) {
		console.log('db connection is already active');
		return;
	}
	if (dbStatus == 0) {
		mongoose.connect(process.env.MONGOOSE_CONNECT, {
			useNewUrlParser: true,
			useUnifiedTopology: true
		});
		db.on('error', console.error.bind(console, 'connection error'));

		db.on('open', () => console.log('connected to mongodb'));

		dbStatus = 1;
	}
};

// export const dbDisconnect = async () => {
// 	if (dbStatus == 0) return;
// 	await mongoose.disconnect();
// 	dbStatus = 0;
// 	console.log('database is disconnected');
// };

export const dbDisconnect = async () => {
	return;
};

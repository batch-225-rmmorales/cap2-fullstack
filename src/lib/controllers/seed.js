import User from '$models/user';
import Item from '$models/item';
import Message from '$models/message';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';

export const seedDB = async () => {
	await dbConnect();

	//SEED USERS
	await User.deleteMany();

	// create super admin
	let newUser = new User({
		username: 'superadmin',
		email: 'super@admin.com',
		password: bcrypt.hashSync('Test123456!', 10),
		imgUrl:
			'https://www.looper.com/img/gallery/the-untold-truth-of-v-for-vendetta/l-intro-1645537694.jpg',
		mobileNo: '+639181234567',
		address: '123 Home',
		isAdmin: true
	});
	await newUser.save();
	// create admin
	newUser = new User({
		username: 'Mors',
		email: 'mors@mors.com',
		password: bcrypt.hashSync('Test1234!', 10),
		imgUrl: 'https://i.ibb.co/phFwY6D/dogpic.jpg',
		mobileNo: '+639181234567',
		address: '123 Home',
		isAdmin: true
	});
	await newUser.save();

	let images = [
		'https://randomuser.me/api/portraits/men/0.jpg',
		'https://randomuser.me/api/portraits/men/1.jpg',
		'https://randomuser.me/api/portraits/men/2.jpg',
		'https://randomuser.me/api/portraits/men/3.jpg',
		'https://randomuser.me/api/portraits/men/4.jpg',
		'https://randomuser.me/api/portraits/men/5.jpg',
		'https://randomuser.me/api/portraits/men/6.jpg',
		'https://randomuser.me/api/portraits/men/7.jpg',
		'https://randomuser.me/api/portraits/men/8.jpg',
		'https://randomuser.me/api/portraits/men/9.jpg',
		'https://randomuser.me/api/portraits/men/10.jpg',
		'https://randomuser.me/api/portraits/women/0.jpg',
		'https://randomuser.me/api/portraits/women/1.jpg',
		'https://randomuser.me/api/portraits/women/2.jpg',
		'https://randomuser.me/api/portraits/women/3.jpg',
		'https://randomuser.me/api/portraits/women/4.jpg',
		'https://randomuser.me/api/portraits/women/5.jpg',
		'https://randomuser.me/api/portraits/women/6.jpg',
		'https://randomuser.me/api/portraits/women/7.jpg',
		'https://randomuser.me/api/portraits/women/8.jpg',
		'https://randomuser.me/api/portraits/women/9.jpg',
		'https://randomuser.me/api/portraits/women/10.jpg'
	];
	// seed user collection
	let response = await fetch('https://jsonplaceholder.typicode.com/users');
	let seed = await response.json();
	console.log(seed);
	seed.forEach(async (item) => {
		var randomImage = images[Math.floor(Math.random() * images.length)];
		newUser = new User({
			username: item.username,
			email: item.email,
			imgUrl: randomImage,
			password: bcrypt.hashSync('Test1234!', 10),
			mobileNo: item.phone,
			address: item.address.street
		});
		await newUser.save();
	});
	console.log('3');
	// SEED ITEMS
	// delete all users
	await Item.deleteMany();

	// seed user collection
	response = await fetch('https://dummyjson.com/products?limit=100');
	let preseed = await response.json();
	seed = preseed.products;
	console.log(seed);
	let usernames = [
		'Bret',
		'Samantha',
		'Antonette',
		'Elwyn.Skiles',
		'Maxime_Nienow',
		'Leopoldo_Corkery',
		'Delphine',
		'Moriah.Stanton',
		'Kamren',
		'Karianne'
	];

	seed.forEach(async (item) => {
		var randomUser = usernames[Math.floor(Math.random() * usernames.length)];
		let newItem = new Item({
			seller: randomUser,
			name: item.title,
			description: item.description,
			imgUrl: item.thumbnail,
			category: item.category,
			price: item.price * 60,
			status: 'For Sale'
		});
		await newItem.save();
	});

	// SEED MESSAGES
	await Item.deleteMany();

	// seed user collection
	response = await fetch('https://dummyjson.com/comments?limit=340');
	preseed = await response.json();
	seed = preseed.comments;
	console.log(seed);
	// https://stackoverflow.com/questions/19590865/from-an-array-of-objects-extract-value-of-a-property-as-array
	let itemArray = await Item.find({});
	let itemIds = itemArray.map((a) => a._id);
	// let userArray = await User.find({});
	usernames = [
		'Bret',
		'Samantha',
		'Antonette',
		'Elwyn.Skiles',
		'Maxime_Nienow',
		'Leopoldo_Corkery',
		'Delphine',
		'Moriah.Stanton',
		'Kamren',
		'Karianne'
	];
	seed.forEach(async (message) => {
		var randomId = itemIds[Math.floor(Math.random() * itemIds.length)];
		var randomUser = usernames[Math.floor(Math.random() * usernames.length)];
		let newMessage = new Message({
			username: randomUser,
			toID: randomId,
			message: message.body
		});
		// add last message as an itemId (so it can also be commented on)
		itemIds.push(newMessage._id);
		try {
			await newMessage.save();
		} catch (err) {
			console.log(err);
		}
	});

	return JSON.stringify({ message: 'Database Reset & Seeded' });
};

import jwt from 'jsonwebtoken';
import * as dotenv from 'dotenv';
dotenv.config();
let ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET;

export const extractJWT = (str) => {
	let result = str.substring(str.indexOf('jwt=') + 4, str.length);
	result = result.split(' ')[0];
	return result;
};

export const extractPayload = (token) => {
	return jwt.verify(token, ACCESS_TOKEN_SECRET);
};
export const isUser = (token) => {
	try {
		let payload = jwt.verify(token, ACCESS_TOKEN_SECRET);
		return true;
	} catch (err) {
		console.log(err);
		return false;
	}
};

export const isAdmin = (token) => {
	try {
		let payload = jwt.verify(token, ACCESS_TOKEN_SECRET);
		if (payload.isAdmin == true) {
			return true;
		} else {
			return false;
		}
	} catch (err) {
		console.log(err);
		return false;
	}
};

export const isSuperAdmin = (token) => {
	try {
		console.log(token);
		let payload = jwt.verify(token, ACCESS_TOKEN_SECRET);
		if (payload.isAdmin == true && payload.username == 'superadmin') {
			return true;
		} else {
			return false;
		}
	} catch (err) {
		console.log(err);
		return false;
	}
};

import { json } from '@sveltejs/kit';
import { dbConnect, dbDisconnect } from '$lib/database';
import User from '$models/user';
import { extractPayload, extractJWT, isUser, isAdmin, isSuperAdmin } from '$lib/verify';

export const GET = async (event) => {
	let cookieData = event.request.headers.get('cookie');
	let jwtToken = extractJWT(cookieData);
	return json(extractPayload(jwtToken));
};

import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect, dbDisconnect } from '$lib/database';
import User from '$models/user';

export const POST = async (event) => {
	const data = await event.request.formData();
	const email = data.get('email');
	const password = data.get('password');
	const username = data.get('username');
	const mobileNo = data.get('mobileNo');
	const address = data.get('address');

	await dbConnect();
	await User.create({
		username,
		email,
		password: bcrypt.hashSync(password, 10),
		mobileNo,
		address
	});
	await dbDisconnect();

	return json({ message: 'user successfully created' });
};

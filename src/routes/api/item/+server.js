import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import { extractJWT, isUser, isAdmin, isSuperAdmin } from '$lib/verify';
import Item from '$models/item';
import { extractPayload } from '$lib/verify';
// import { verify } from 'jsonwebtoken';

export const GET = async (event) => {
	await dbConnect();
	let items = await Item.find({}).lean();

	items = JSON.parse(JSON.stringify(items));
	items = items.reverse();
	return json(items);
};

//https://www.reddit.com/r/sveltejs/comments/s9wfxj/sveltekit_how_to_read_cookie_inside/
export const DELETE = async (event) => {
	// this is how you access cookie; need to find cleaner way without helper function
	let cookieData = event.request.headers.get('cookie');
	let jwtToken = extractJWT(cookieData);
	console.log(isUser(jwtToken));

	// this is how you access the body BOOM!
	// https://stackoverflow.com/questions/66913326/getting-the-raw-body-of-a-request-to-a-sveltekit-endpoint
	const rawBody = await event.request.text();
	const body = JSON.parse(rawBody);
	console.log(body);

	if (isAdmin(jwtToken)) {
		await dbConnect();
		let items = await Item.deleteOne({ _id: body._id });

		items = JSON.parse(JSON.stringify(items));
		return json(items);
	}
};

export const PATCH = async (event) => {
	// this is how you access cookie; need to find cleaner way without helper function
	let cookieData = event.request.headers.get('cookie');
	let jwtToken = extractJWT(cookieData);
	console.log(isUser(jwtToken));
	let payload = extractPayload(jwtToken);

	// this is how you access the body BOOM!
	// https://stackoverflow.com/questions/66913326/getting-the-raw-body-of-a-request-to-a-sveltekit-endpoint
	const rawBody = await event.request.text();
	const body = JSON.parse(rawBody);
	console.log(body);
	console.log(payload);

	if (body.update.buyer != payload.username) {
		return json({ message: 'failed to patch' });
	}
	await dbConnect();
	let toUpdate = await Item.findOne(body.filter);
	let response = await toUpdate.update(body.update);

	let data = JSON.parse(JSON.stringify(response));
	return json(data);
};

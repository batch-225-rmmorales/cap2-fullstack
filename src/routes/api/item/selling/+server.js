import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import { extractJWT, extractPayload, isUser, isAdmin, isSuperAdmin } from '$lib/verify';
import Item from '$models/item';
// import { verify } from 'jsonwebtoken';

export const GET = async (event) => {
	let cookieData = event.request.headers.get('cookie');
	let jwtToken = extractJWT(cookieData);
	console.log(isUser(jwtToken));
	let payload = extractPayload(jwtToken);
	await dbConnect();
	let items;

	items = await Item.find({ seller: payload.username, status: 'For Sale' }).lean();

	items = JSON.parse(JSON.stringify(items));
	items = items.reverse();
	return json(items);
};

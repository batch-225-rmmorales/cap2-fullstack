import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import { extractJWT, isUser, isAdmin, isSuperAdmin } from '$lib/verify';
import Item from '$models/item';
import { extractPayload } from '$lib/verify';
// import { verify } from 'jsonwebtoken';

export const PATCH = async (event) => {
	// this is how you access cookie; need to find cleaner way without helper function
	let cookieData = event.request.headers.get('cookie');
	let jwtToken = extractJWT(cookieData);
	console.log(isUser(jwtToken));
	let payload = extractPayload(jwtToken);

	// this is how you access the body BOOM!
	// https://stackoverflow.com/questions/66913326/getting-the-raw-body-of-a-request-to-a-sveltekit-endpoint
	const rawBody = await event.request.text();
	const body = JSON.parse(rawBody);
	console.log(body);

	if (isAdmin(jwtToken) || body.update.seller != payload.username) {
		await dbConnect();
		let toUpdate = await Item.findOne(body.filter);
		let response = await toUpdate.update(body.update);
	} else {
		return json({ message: 'failed to patch' });
	}

	let data = JSON.parse(JSON.stringify(response));
	return json(data);
};

import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import { extractJWT, isUser, isAdmin, isSuperAdmin } from '$lib/verify';
import Item from '$models/item';
// import { verify } from 'jsonwebtoken';

export const GET = async (event) => {
	let cookieData = event.request.headers.get('cookie');
	let category = event.url.searchParams.get('category');
	let status = event.url.searchParams.get('status');
	let seller = event.url.searchParams.get('seller');
	console.log(category);
	let jwtToken = extractJWT(cookieData);
	await dbConnect();
	let items;
	let filterAdmin;
	let filterUser;
	console.log(seller);
	if (!seller || seller == '') {
		if (category || category == '') {
			if (!status) {
				filterAdmin = { category: category };
			} else {
				filterAdmin = { category: category, status: status };
			}
			filterUser = { category: category, status: 'For Sale' };
		} else {
			if (!status) {
				filterAdmin = {};
			} else {
				filterAdmin = { status: status };
			}
			filterUser = { status: 'For Sale' };
		}
	} else {
		if (!status) {
			filterAdmin = { seller: seller };
		} else {
			filterAdmin = { seller: seller, status: status };
		}
		filterUser = { seller: seller, status: 'For Sale' };
	}

	if (isAdmin(jwtToken) || isSuperAdmin(jwtToken)) {
		items = await Item.find(filterAdmin).lean();
	} else {
		items = await Item.find(filterUser).lean();
	}

	items = JSON.parse(JSON.stringify(items));
	items = items.reverse();
	return json(items);
};

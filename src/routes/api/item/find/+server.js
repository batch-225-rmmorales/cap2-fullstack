import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import { extractJWT, isUser, isAdmin, isSuperAdmin } from '$lib/verify';
import Item from '$models/item';
// import { verify } from 'jsonwebtoken';

export const GET = async (event) => {
	let cookieData = event.request.headers.get('cookie');
	console.log('event is');
	console.log(event.request.url);
	let item = event.request.url.split('=')[1];
	// let item = event.url.searchParams.get('item');
	let jwtToken = extractJWT(cookieData);
	await dbConnect();
	let items;
	let filter;

	if (item || item != '') {
		filter = { _id: item };
	}
	console.log('filter is');
	console.log(filter);
	items = await Item.findOne(filter);
	console.log(items);
	items = JSON.parse(JSON.stringify(items));

	return json(items);
};

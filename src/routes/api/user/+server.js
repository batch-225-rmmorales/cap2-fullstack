import { json } from '@sveltejs/kit';
import { dbConnect, dbDisconnect } from '$lib/database';
import User from '$models/user';

export const GET = async (event) => {
	await dbConnect();
	let users = await User.find({}, [], { sort: { isAdmin: -1 } }).lean();
	await dbDisconnect();

	users = JSON.parse(JSON.stringify(users));
	// users = users.reverse();
	return json(users);
};

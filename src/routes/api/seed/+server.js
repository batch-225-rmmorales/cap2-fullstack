import { json } from '@sveltejs/kit';
import { dbConnect, dbDisconnect } from '$lib/database';
import { seedDB } from "$lib/controllers/seed";

export const DELETE = async (event) => {
    await dbConnect();
    console.log("mors");
    let response = await seedDB();
    return new Response(response);
};

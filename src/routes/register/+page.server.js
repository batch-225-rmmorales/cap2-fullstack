import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import User from '$models/user';
import { fail, redirect } from '@sveltejs/kit';

/** @type {import('./$types').Actions} */
export const actions = {
	default: async (event) => {
		let username;
		let password;
		let mobileNo;
		let imgUrl;
		let email;
		await event.request.formData().then((data) => {
			email = data.get('email');
			password = data.get('password');
			username = data.get('username');
			mobileNo = data.get('mobileNo');
			imgUrl = data.get('imgUrl');
		});

		await dbConnect();
		await User.create({
			username,
			email,
			password: bcrypt.hashSync(password, 10),
			mobileNo,
			imgUrl
		});
		// await dbDisconnect();

		throw redirect(302, '/login');
	}
};

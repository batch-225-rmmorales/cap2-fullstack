import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import User from '$models/user';
import jwt from 'jsonwebtoken';
import { fail, redirect } from '@sveltejs/kit';

import * as dotenv from 'dotenv';
dotenv.config();
let ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET;

/** @type {import('./$types').Actions} */
export const actions = {
	default: async (event) => {
		console.log(ACCESS_TOKEN_SECRET);

		let username;
		let password;
		await event.request.formData().then((data) => {
			password = data.get('password');
			username = data.get('username');
		});

		await dbConnect();
		const user = await User.findOne({
			username: username
		});

		if (!user) {
			return json({
				message: 'Login not successful',
				error: 'User not found'
			});
		} else {
			const maxAge = 3 * 60 * 60; // 3 hours in seconds
			let passwordCheck = bcrypt.compareSync(password, user.password);
			if (passwordCheck) {
				let payload = {
					username: user.username,
					isAdmin: user.isAdmin,
					userID: user._id
				};
				const accessToken = jwt.sign(payload, ACCESS_TOKEN_SECRET, {
					expiresIn: maxAge
				});
				// if (!res.cookie) res.cookie = {};
				// console.log(res.cookie);

				event.cookies.set('jwt', accessToken, {
					// send cookie for every page
					path: '/',
					// server side only cookie so you can't use `document.cookie`
					httpOnly: true,
					// only requests from same site can send cookies
					// https://developer.mozilla.org/en-US/docs/Glossary/CSRF
					sameSite: 'strict',
					// only sent over HTTPS in production
					// secure: process.env.NODE_ENV === 'production',
					// set cookie to expire after a month
					maxAge: 60 * 60 * 3
				});
				// res.send(accessToken);
				// console.log('checkpoint 3');
				throw redirect(302, '/marketplace');
			}
		}
	}
};

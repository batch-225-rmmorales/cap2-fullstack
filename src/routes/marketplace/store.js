import { writable, derived } from 'svelte/store';

/** Store for your data. 
This assumes the data you're pulling back will be an array.
If it's going to be an object, default this to an empty object.
**/
export const apiData = writable([]);
export const marketplace = writable([]);
export const purchased = writable([]);
export const selling = writable([]);
export const sold = writable([]);
export const payload2 = writable([]);
export const users = writable([]);

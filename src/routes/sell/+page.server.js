import { json } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { dbConnect } from '$lib/database';
import Item from '$models/item';
import { fail, redirect } from '@sveltejs/kit';
import { extractPayload, extractJWT, isUser, isAdmin, isSuperAdmin } from '$lib/verify';

/** @type {import('./$types').Actions} */
export const actions = {
	default: async (event) => {
		let name;
		let description;
		let imgUrl;
		let price;
		let category;

		let cookieData = event.request.headers.get('cookie');
		let jwtToken = extractJWT(cookieData);
		console.log(isUser(jwtToken));
		let payload = extractPayload(jwtToken);

		await event.request.formData().then((data) => {
			name = data.get('name');
			description = data.get('description');
			imgUrl = data.get('imgUrl');
			price = data.get('price');
			category = data.get('category');
		});

		await dbConnect();
		console.log(payload.username);
		await Item.create({
			seller: payload.username,
			name: name,
			description: description,
			imgUrl: imgUrl,
			category: category,
			price: price,
			status: 'For Sale'
		});
		// await dbDisconnect();

		throw redirect(302, '/marketplace');
	}
};

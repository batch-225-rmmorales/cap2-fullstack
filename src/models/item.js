// requires
import mongoose from 'mongoose';

const itemSchema = new mongoose.Schema({
	seller: {
		type: String
		// required: [true, "userID is required"],
	},
	buyer: {
		type: String
	},
	name: {
		type: String,
		required: [true, 'Item name is required']
	},
	description: {
		type: String,
		required: [true, 'Description is required']
	},
	category: {
		type: String
	},
	imgUrl: {
		type: String,
		required: [true, 'image is required']
	},
	price: {
		type: Number,
		required: [true, 'Price is required']
	},
	status: String,
	iat: {
		type: Date,
		// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
		default: new Date()
	}
});

export default mongoose.model('Item', itemSchema);

//create the schema,  model and export the file

import mongoose from 'mongoose';

const messageSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, 'fromID is required']
	},
	toID: {
		type: String
	},
	message: {
		type: String,
		required: [true, 'message is required']
	},

	iat: {
		type: Date,
		// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	isParent: {
		type: Boolean,
		default: false
	}
});

// module.exports is the way for node js to treat this value as package
export default mongoose.model('Message', messageSchema);

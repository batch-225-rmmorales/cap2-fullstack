//create the schema,  model and export the file

import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, 'username is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	imgUrl: {
		type: String,
		required: [true, 'image is required']
	},
	address: {
		type: String
	},

	mobileNo: {
		type: String
	},

	isAdmin: {
		type: Boolean,
		default: false
	}
});

export default mongoose.model('User', userSchema);

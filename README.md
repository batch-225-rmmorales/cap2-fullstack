# Capstone 2: Full stack P2P Ecommerce app
[Deployed App](https://cap2-fullstack.vercel.app/)
## Features

- Authentication/Cookies

### User

- Registration
- View 'For Sale' Products + Pagination
- Sections for 'My Items for Sale', 'My Sold Items', 'My Purchased Items'
- Create Product; Edit/Archive own Product
- Filter by category, View Seller + Seller shop
- Contact Detail protection/privacy

### Admin

- View 'All Products' including 'Sold' and 'Archived'
- Filter by Status
- Archive Product
- Admin/Users side panel
- Make Admin

### SuperAdmin

- Remove Admin

### For improvement

- Message/Commenting System
- Auth Library/Module
- Component Library
- Single Page Application / Progressive Web App
- External API Integration

## for tomorrow

- add product ✔️
- pagination ✔️
- promote to admin ✔️
- demote to user ✔️
- delete user 🚧
- commenting ❌
- https://www.okupter.com/blog/sveltekit-query-parameters

## resources

- https://joyofcode.xyz/sveltekit-authentication-using-cookies
- profile ✔️
- marketplace ✔️
- admin ✔️
- login ✔️ (no message yet if failed; no logout yet)
- register 🚧

## todo

- load mo category para makuha mo smartphones lang etc (tanggal mo na ung featured?) ✔️
- ano ba status natin ✔️
  - for sale, sold, archived ✔️
- do featured category like olx ❌
